/*
This is the c configuration file for the keymap

Copyright 2012 Jun Wako <wakojun@gmail.com>
Copyright 2015 Jack Humbert

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

/* Select hand configuration */

// #define MASTER_LEFT
// #define MASTER_RIGHT
#define EE_HANDS

#define TAPPING_FORCE_HOLD
#define TAPPING_TERM 100

/* Encoder */
#define ENCODERS_PAD_A { F4 }
#define ENCODERS_PAD_B { F5 }
#define ENCODER_RESOLUTIONS { 4 }
#define ENCODERS_PAD_A_RIGHT { F4 }
#define ENCODERS_PAD_B_RIGHT { F5 }
#define ENCODER_RESOLUTIONS_RIGHT { 4 }

#undef RGBLED_NUM

// Not compatible with -flto flag
#define NO_ACTION_MACRO
#define NO_ACTION_FUNCTION

// Redefine HID parameters
#define RAW_USAGE_PAGE 0xFF60
#define RAW_USAGE_ID 0x61

#undef OLED_FONT_H
#define OLED_FONT_H "keyboards/lily58/keymaps/KirmTwinty/hid/firmware/font/glcdfont_hid.c"
#define SPLIT_OLED_ENABLE

#define SPLIT_TRANSACTION_IDS_USER RPC_ID_USER_SCREEN_2
#define RPC_M2S_BUFFER_SIZE 84
/*
#define RGBLIGHT_ANIMATIONS
#define RGBLED_NUM 27
#define RGBLIGHT_LIMIT_VAL 120
#define RGBLIGHT_HUE_STEP 10
#define RGBLIGHT_SAT_STEP 17
#define RGBLIGHT_VAL_STEP 17
*/
// Underglow
/*
#undef RGBLED_NUM
#define RGBLED_NUM 14    // Number of LEDs
#define RGBLIGHT_ANIMATIONS
#define RGBLIGHT_SLEEP
*/
