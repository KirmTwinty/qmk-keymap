#include QMK_KEYBOARD_H
#include "raw_hid.h"
#include "transactions.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "animations/bored.h"
#include <string.h>

// For AZERTY -- force symbols on lower and raise to be the right ones
#include "keymap_extras/keymap_french.h"

#define N_ROWS 4
#define N_COLS 21
// maybe it could be a per module variable?
char screen_text_1[N_ROWS][N_COLS];
char screen_text_2[N_ROWS][N_COLS];

#define	MODULE_TIMER_LIMIT 2000 // no data received during 2s

bool cpu_module = false;
uint16_t cpu_module_timer;

bool media_module = false;
uint16_t media_module_timer;

void set_data(uint8_t *data, uint8_t length, uint8_t screen, uint8_t i_line){
  // update data string
  if (screen == 0) {
    cpu_module = true;
    cpu_module_timer = timer_read();
    memcpy(&screen_text_1[i_line], data + 2, N_COLS);
    for(int i=0; i < N_ROWS; i++)
      dprintf("\tValue: %s\n", screen_text_1[i]);
  }else{
    media_module = true;
    media_module_timer = timer_read();
    memcpy(&screen_text_2[i_line], data + 2, N_COLS);
  }
}
void oled_render_data(uint8_t screen) {
  if (screen == 0) {
    for(int i=0; i < N_ROWS; i++){
      oled_set_cursor(0, i);
      oled_write(screen_text_1[i], false);
    }
  }else{
    for(int i=0; i < N_ROWS; i++){
      oled_set_cursor(0, i);
      oled_write(screen_text_2[i], false);
    }
  }
}


enum layer_number {
  _QWERTY = 0,
  _LOWER,
  _RAISE,
  _ADJUST,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* QWERTY
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ESC  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  `   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |RAlt |   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |   ;  |  '   |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |LOWER|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |   ,  |   .  |   /  |RAISE |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LGUI | LAlt |LCTRL | /Enter  /       \Space \  |BackSP |RShift|DEL   |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

 [_QWERTY] = LAYOUT(
  KC_ESC,      KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                     KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_GRV,
  KC_TAB,      KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_MINS,
  KC_RALT,    KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
  MO(_LOWER),  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_LBRC,  KC_RBRC,  KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, MO(_RAISE),
                        KC_LGUI, KC_LALT, KC_LEFT_CTRL, KC_ENT, KC_SPC, KC_BSPC, KC_RSFT, KC_DEL
),
/* LOWER
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |                    |  F7  |  F8  |  F9  | F10  | F11  | F12  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   ~  |   <  |   >  |   #  |   $  |   %  |-------.    ,-------|   ^  |   &  |   *  |   (  |   )  |   [  |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |   _  |   +  |   {  |   }  |   ]  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
[_LOWER] = LAYOUT(
  _______, _______, _______, _______, _______, _______,                   _______, _______, _______,_______, _______, _______,
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                     KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,
  FR_TILD, FR_LABK, FR_RABK,   FR_HASH, FR_DLR,  FR_PERC,                   FR_CIRC, FR_AMPR, FR_ASTR, FR_LPRN, FR_RPRN, FR_LBRC,
  _______, _______, _______, _______, _______, _______, _______, _______, FR_DIAE, FR_UNDS, FR_PLUS, FR_LCBR, FR_RCBR, FR_RBRC,
                             _______, _______, _______, _______, _______,  _______, _______, _______
),

/* [_LOWER] = LAYOUT( */
/*   _______, _______, _______, _______, _______, _______,                   _______, _______, _______,_______, _______, _______, */
/*   KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                     KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12, */
/*   KC_GRV, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,                   KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_TILD, */
/*   _______, _______, _______, _______, _______, _______, _______, _______, XXXXXXX, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE, */
/*                              _______, _______, _______, _______, _______,  _______, _______, _______ */
/* ), */
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |-------.    ,-------|      | Left | Down |  Up  |Right |      |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |  F7  |  F8  |  F9  | F10  | F11  | F12  |-------|    |-------|   +  |   -  |   =  |   [  |   ]  |   \  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

[_RAISE] = LAYOUT(
  _______, _______, _______, _______, _______, _______,                     _______, _______, _______, _______, _______, _______,
  KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                        KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    _______,
  KC_F1,  KC_F2,    KC_F3,   KC_F4,   KC_F5,   KC_F6,                       XXXXXXX, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, XXXXXXX,
  KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,   _______, _______,  KC_PLUS, KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_BSLS,
                             _______, _______, _______,  _______, _______,  _______, _______, _______
),
/* ADJUST
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------.    ,-------|      |      |RGB ON| HUE+ | SAT+ | VAL+ |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |      | MODE | HUE- | SAT- | VAL- |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_ADJUST] = LAYOUT(
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                             _______, _______, _______, _______, _______,  _______, _______, _______
  )
};

#if defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][1] = {
    [_BASE] =   { ENCODER_CCW_CW(KC_VOLD, KC_VOLU)  },
    [_LOWER] =  { ENCODER_CCW_CW(KC_MS_WH_UP, KC_MS_WH_DOWN)  },
    [_RAISE] =  { ENCODER_CCW_CW(RGB_SPD, RGB_SPI)  },
    [_ADJUST] = { ENCODER_CCW_CW(KC_RIGHT, KC_LEFT) },
};
#endif

bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index == 0) { /* First encoder */
        if (clockwise) {
            tap_code_delay(KC_VOLU, 10);
        } else {
            tap_code_delay(KC_VOLD, 10);
        }
    }
    return false;
}

layer_state_t layer_state_set_user(layer_state_t state) {
  return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
}

//SSD1306 OLED update loop, make sure to enable OLED_ENABLE=yes in rules.mk
#ifdef OLED_ENABLE
oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_keyboard_master())
    return OLED_ROTATION_180;  // flips the display 180 degrees if offhand

  return rotation;
}

// When you add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
void set_keylog(uint16_t keycode, keyrecord_t *record);
const char *read_keylog(void);
const char *read_keylogs(void);

// const char *read_mode_icon(bool swap);
// const char *read_host_led_state(void);
// void set_timelog(void);
// const char *read_timelog(void);

// OLED Animation
/* bool first_render = true; */
/* uint8_t frame_number = 0; */
/* static void render_anim(void) { */
/*     if (first_render) { */
/*         oled_write_raw_P( frame, ANIM_SIZE); */
/*         first_render = false; */
/*     } else { */
/*       change_frame_bytewise(frame_number); */
/*     } */
/*     frame_number = frame_number+1 > IDLE_FRAMES ? 0 : frame_number+1; */
/* } */


// OLED Animation BORED
bool first_render = true;
uint8_t frame_number = 0;
uint8_t temporize_first_frame = 0;
uint8_t TEMPORIZATION = 200;
static void render_anim(void) {
    if (first_render) {
        oled_write_raw_P( frame, ANIM_SIZE);
        first_render = false;
    } else {
      if(frame_number > 0){
        change_frame_bytewise(frame_number);
	frame_number = frame_number+1 > IDLE_FRAMES ? 0 : frame_number+1;

      }else if(frame_number == 0 && temporize_first_frame > TEMPORIZATION){
        change_frame_bytewise(frame_number);
	frame_number = frame_number+1 > IDLE_FRAMES ? 0 : frame_number+1;
	temporize_first_frame = 0;
	TEMPORIZATION = rand() % 200;
      }
      else{
	temporize_first_frame++;
      }
    }
}



bool oled_task_user(void) {
  if (is_keyboard_master()) {
    if(cpu_module && timer_elapsed(cpu_module_timer) < MODULE_TIMER_LIMIT){
      oled_render_data(0);
    }else{
      // display default screen
      oled_write(read_logo(), false);
      oled_write_ln("                    ", false);
      // oled_write_ln(read_layer_state(), false);
      // oled_write_ln(read_keylog(), false);
      // oled_write_ln(read_keylogs(), false);
      //oled_write_ln(read_mode_icon(keymap_config.swap_lalt_lgui), false);
      //oled_write_ln(read_host_led_state(), false);
      //oled_write_ln(read_timelog(), false);
    }
  } else {
    if(media_module && timer_elapsed(media_module_timer) < MODULE_TIMER_LIMIT){
      oled_render_data(1);
    }else{
      render_anim();
    }
  }
  return false;
}

// Sync screen 2 data with slave
static uint32_t last_sync = 0;
void housekeeping_task_user(void) {
  if (is_keyboard_master()) {

      // Interact with slave every 500ms
    // if (timer_elapsed32(last_sync) > 500) {
    if (timer_elapsed32(last_sync) > 2000) {
      dprint("Inside housekeeping\n");
      dprintf("Syncing: %s\n", screen_text_2);
	if(transaction_rpc_send(RPC_ID_USER_SCREEN_2, sizeof(char) * N_ROWS * N_COLS, &screen_text_2)) {
	  last_sync = timer_read32();
	} else {
	  dprint("Slave sync failed!\n");
	}
      }
  }
}
void user_sync_a_slave_handler(uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data) {
  memcpy(&screen_text_2, in_data, in_buflen);
  /* const char *second_screen = (const char*) in_data; */
  /* for(int i=0; i < N_ROWS; i++) */
  /*   for(int j=0; j < N_COLS; j++) */
  /*     screen_text_2[i][j] = *(second_screen + j * N_ROWS + i); */
  media_module = true;
  media_module_timer = timer_read();
}
void keyboard_post_init_user(void) {
    transaction_register_rpc(RPC_ID_USER_SCREEN_2, user_sync_a_slave_handler);
    #ifdef CONSOLE_ENABLE
    debug_enable=true;
    debug_matrix=true;
    #endif
}
#endif // OLED_ENABLE

#ifdef RAW_ENABLE
void raw_hid_receive(uint8_t *data, uint8_t length){
  // Your code goes here. data is the packet received from host.s
  uint8_t screen = data[0] - 1;
  uint8_t i_line = data[1] - 1;
  dprintf("Setting screen %d, line %d\n", screen, i_line);
  set_data(data, length, screen, i_line);
  raw_hid_send(data, length); // Debug by sending it back
}
#endif // RAW_ENABLE

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
#ifdef OLED_ENABLE
    set_keylog(keycode, record);
#endif
    // set_timelog();
  }
  return true;
}
