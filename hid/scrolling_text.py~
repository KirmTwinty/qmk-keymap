import time

class ScrollingText:
    """Text that automatically scroll.

    Defines a text that will scroll with
    a given frequency and a given width.

    Any text will then be of the given
    width.
    The text will then scroll within this
    fixed width and frequency.

    Attributes
    ----------
    width : int
       fixed width of the text
    frequency : float
       scrolling frequency (Hz)

    Methods
    -------
    Methods

    """
    def __init__(self, text, width, frequency=0.5):
        "docstring"
        self.text = text
        self.width = width
        self._count = 0
        self._increment = 1
        self.frequency = frequency
        self._last_time = time.time()

    def __str__(self):
        if len(self.text) <= self.width:
            return self.text

        t = time.time()
        if abs(self._last_time - t) >= 1 / self.frequency:
            # check if count is going up or down
            if self._count + self._increment + self.width > len(self.text):
                self._increment = -1
            elif self._count + self._increment < 0:
                self._increment = 1
            self._count = self._count + self._increment
            self._last_time = t
        return self.text[self._count:(self._count + self.width)]

if __name__ == "__main__":
    s = ScrollingText('1234567890' * 1, 5)
    for i in range(50):
        print(s)
        time.sleep(0.5)
