import hid

class HIDDevice(hid.Device):
    """Override the default `hid.Device` class

    The default class is overriden because it doesn't provide
    some features such as accessing a vid:pid with usage page,
    directly writting lists (always need binary objects), etc.

    This class is just an interface to a more convenient way
    of using `hid.Device`

    Attributes
    ----------
    vendor_id : int
        vendor id
    product_id : int
        product id
    serial : int (optionnal)
        serial
    path : bytes (optionnal)
        hid path to the product
    usage_page : int (optionnal)
        usage page to use for the product

    Methods
    -------
    Methods

    """
    def __init__(self, vendor_id, product_id, serial=None, path=None, usage_page=None):
        "Init a HID device with given vendor_id and product_id"
        self.vendor_id = vendor_id
        self.product_id = product_id
        self.usage_page = usage_page
        if usage_page and not path:
            for el in hid.enumerate(vendor_id, product_id):
                if el['usage_page'] == usage_page:
                    path = el['path']

        super().__init__(vendor_id, product_id, serial, path)

    def write(self, data):
        """Override the `write` method to accept different
        types of data.

        `data` can now be a list of integers to send or
        directly a string.

        Parameters
        ----------
        data : list, str
            data to be sent

        """
        if isinstance(data,list):
            data = bytes(data)
        elif isinstance(data, str):
            data = data.encode()
        else:
            raise TypeError('Wrong input data type: str or list expected.')

        super().write(data)
