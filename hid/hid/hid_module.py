import time
import threading
import functools
from conf import logging

log = logging.getLogger('HID:module')

MODULES = []
MODULES_KEYS = []


def register(cls):
    "Register the module to the list of available ones."
    if cls not in MODULES:
        if cls.RESERVED_KEY not in MODULES_KEYS:
            MODULES.append(cls)
            MODULES_KEYS.append(cls.RESERVED_KEY)
        else:
            log.error(f'Key {cls.RESERVED_KEY} already reserved for\
            {MODULES[MODULES_KEYS.index(cls.RESERVED_KEY)].__name__}')


class HIDModuleMetaClass(type):
    """MetaClass to register all defined modules globally.
    """
    def __new__(cls, clsname, bases, attrs):
        newclass = super().__new__(cls, clsname, bases, attrs)
        register(newclass)
        return newclass


class HIDModule(threading.Thread, metaclass=HIDModuleMetaClass):
    """Create a custom HID Module.

    This is the base class to create your custom HID module.

    Your new defined module needs to inherits from this class
    and override the following functions:
    - `update`: called undefinitely based on module frequency
    - `get_status`: check if `get_lines` should be called
    - `get_lines`: returns the lines to display

    For `get_lines`, a tuple is returned, it represents the
    text in first position and its adjustment in second which
    can be ('ljust' or 'rjust')

    Moreover, your module needs to implement the associated
    `C_CODE`, `INCLUDE` and `RESERVED_CODE`.
    - `C_CODE` is the code to be generated in the main
      `switch` clause in the `C` code.
    - `INCLUDE` represents the extra file to be included in
      the QMK `keymap.c` file.
    - `RESERVED_KEY` is the key used in the `switch` clause
      in the `C` code. It should be unique to any module.

    Note
    ----
    The `get_lines` function should returns an array of size
    `conf.N_ROWS`.
    Each line is itself an array made of either `str` or
    `scrolling_text.Scrollingtext`.
    Any output too long will be cropped by the server
    to make sure that the data sent always correspond
    to `conf.N_ROWS` and `conf.N_COLS`.

    Attributes
    ----------
    sleep_time : int
        time between two fetching.

    Methods
    -------
    get_status()
       True if `get_lines` should be called.
    get_lines(): (list, list)
       Returns the text to be displayed on screen [0] and its adjustment [1].

    """
    C_CODE = None
    INCLUDE = None
    RESERVED_KEY = None

    def __init__(self, frequency=0.2):
        "docstring"
        self.screen = 1
        self._frequency = frequency
        self._last_call = time.time()
        threading.Thread.__init__(self)


    def run(self):
        "Run the module"
        while True:
            t = time.time()
            self.update()
            if t - self._last_call < 1 / self._frequency:
                time.sleep(1 / self._frequency - (t - self._last_call))
            self._last_call = t

    def get(self):
        """This is called from the server to actually get the lines.

        We can do extra processing here in the future
        """
        return self.get_lines()

    def update(self):
        "Update the necessary contents of the module."

    def get_lines(self):
        "Returns the text to be displayed on screen."

    def get_status(self):
        "True if `get_lines` should be called."
