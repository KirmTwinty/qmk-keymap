class CustomChar():
    """Defines a custom symbol

    If you want to setup a specific symbol
    that is non standard (e.g. defined in your own
    font glyph), you will need to force HID to use
    a raw symbol command instead of using ASCII one.

    The format is any of the following:
    - 'int' (e.g. 213)
    - 'hex' (e.g. 0xd5)
    - 'str' (e.g. '\xd5')

    This is done with this class.

    Attributes
    ----------
    code :
        address of the specific symbol
    format : str
        format of the code ('hex', 'str', 'int')
    """
    def __init__(self, code, format='hex'):
        "Create a custom symbol."
        if format == 'str':
            self.code = code  # to int in any case
        else:
            self.code = chr(code)

    def __len__(self):
        return 1

    def __str__(self):
        return self.code
