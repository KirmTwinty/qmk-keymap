
import hid

vid = 0x04d8	# Change it for your device
pid = 0xeb2d	# Change it for your device

with hid.Device(vid, pid, path=b'/dev/hidraw8') as h:
    print(f'Device manufacturer: {h.manufacturer}')
    print(f'Product: {h.product}')
    print(f'Serial Number: {h.serial}')

    byte_array = [1,1,14,32,66,111,100,111,109,32,66,101,97,99,104,32,84,101,114,114,111,114,32]
    byte_array = [1,2,14,32,66,111,100,111,109,32,66,101,97,99,104,32,84,101,114,114,111,114,32]
    byte_array = [1,3,14,32,66,111,100,111,109,32,66,101,97,99,104,32,84,101,114,114,111,114,32]
    byte_array = [1,4,14,32,66,111,100,111,109,32,66,101,97,99,104,32,84,101,114,114,111,114,32]
    h.write(bytes(byte_array))
    print(h.read(len(byte_array), timeout=2000))
