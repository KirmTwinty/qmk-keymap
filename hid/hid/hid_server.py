import os
from hid_device import HIDDevice
from modules.hid_media import HIDMedia
from modules.hid_computer import HIDComputer
import time
import conf
import unicodedata
from hid_module import MODULES
from custom_char import CustomChar
from conf import logging


log = logging.getLogger('HID:server')


class HIDServer:
    """Create a HID server to send data to qmk firmware.

    Create a server that will send the text to be displayed
    on your QMK keyboard.

    Warning
    -------
    Unicode cannot be handle. Instead, ASCII is used and any
    unicode character is replaced by its equivalent in ASCII or `?` otherwise.

    ``` python
    unicode.encode('ascii', 'replace')
    ```

    Attributes
    ----------
    Attributes

    Methods
    -------
    Methods

    """
    def __init__(self, device, modules=MODULES, sleep_time=0.5):
        "Create the server."
        self.sleep_time = sleep_time
        self.device = device
        self.modules = []
        log.info(f'{len(modules)} modules selected out of {len(MODULES)}')
        for module in modules:
            if module in MODULES:
                log.info(f'  Adding module {module.__name__}.')
                self.modules.append(module())
            else:
                log.exception(f'No modules {module.__name__} found! Check that it has been imported and defined with @hid_module.')

        for module in self.modules:
            module.start()
        # self.media = HIDMedia()
        # self.media.start()

    def run(self):
        while True:
            for module in self.modules:
                if module.get_status():
                    if lines := module.get():
                        self.send_lines(lines, module.screen)
            time.sleep(self.sleep_time)

    def send_lines(self, lines, screen=1):
        lines, adjustment = lines
        for i_line, line in enumerate(lines):
            line_str = ''
            for line_el in line:
                if isinstance(line_el, CustomChar):
                    line_str += str(line_el)
                else:
                    line_str += unicodedata.normalize('NFKD', str(line_el)).encode('ascii', 'ignore').decode()

            if len(line_str) > conf.N_COLS:
                log.warning(f'Line {line_str} is too big, cropping.')
                line_str = line_str[:(conf.N_COLS-4)] + ' ...'
            elif len(line_str) < conf.N_COLS:
                if adjustment[i_line] == 'rjust':
                    line_str = line_str.rjust(conf.N_COLS, ' ')
                else:
                    line_str = line_str.ljust(conf.N_COLS, ' ')

            l = [screen, i_line + 1] + [ord(char) for char in line_str]
            self.device.write(l)

if __name__ == "__main__":
    vendor_id = 0x04d8
    product_id = 0xeb2d
    usage_page = 0xff60
    with HIDDevice(vendor_id, product_id, usage_page=usage_page) as h:
        server = HIDServer(h)
        server.run()
