import os
import psutil
import threading
from hid_module import HIDModule
from custom_char import CustomChar

from conf import logging

log = logging.getLogger('HID:computer')

def secs2hours(secs):
    mm, ss = divmod(secs, 60)
    hh, mm = divmod(mm, 60)
    return "%d:%02d:%02d" % (hh, mm, ss)

CPU_ICON = CustomChar(0xd7)
BATTERY_ICON = CustomChar(0xd8)
DISK_ICON = CustomChar(0xd9)
LOAD_AVG_ICON = CustomChar(0xda)
HOURGLASS_ICON = CustomChar(0xdb)

class HIDComputer(HIDModule):
    """Send Computer information through HID
    """

    RESERVED_KEY = 1

    def __init__(self):
        "Create the computer monitoring."
        self.lines = []
        self.check_lines = ''
        self.cpu_freq = None
        self.load_avg = None
        self.disk_usage = None
        self.battery = None
        super().__init__(0.5)
        self.update()


    def get_status(self):
        return True

    def update(self):
        self.cpu_freq = psutil.cpu_freq()
        self.load_avg = [x / psutil.cpu_count() * 100 for x in psutil.getloadavg()]
        self.disk_usage = psutil.disk_usage('/')
        self.battery = psutil.sensors_battery()

    def get_lines(self):
        lines = []
        if self.battery.power_plugged:
            battery_info = 'Charging'
        else:
            battery_info = secs2hours(self.battery.secsleft)

        lines.append([BATTERY_ICON, " ", str(round(self.battery.percent)), '% ', HOURGLASS_ICON, ' - ', battery_info])
        lines.append([CPU_ICON, ' ', str(round(self.cpu_freq.current)),
                      ' \x19', str(round(self.cpu_freq.min)),
                      ' \x18', str(round(self.cpu_freq.max))])
        lines.append([DISK_ICON, " ", str(self.disk_usage.percent), "% used"])
        lines.append([LOAD_AVG_ICON, " ", str(round(self.load_avg[0])), "% ",
                      str(round(self.load_avg[1])), "% ",
                      str(round(self.load_avg[2])), "%",
                      ])

        return (lines, ["ljust"] * 4)
