from gi.repository import GObject as gobject
import dbus.mainloop.glib
import os
import time
import conf
import dbus
import threading
import traceback
from datetime import timedelta
from hid_module import HIDModule
from custom_char import CustomChar

from conf import logging
from scrolling_text import ScrollingText

log = logging.getLogger('HID:server')

USE_CUSTOM_FONT = True

if USE_CUSTOM_FONT:
    ARTIST_TITLE = CustomChar(0xd5)
    ALBUM_TITLE = CustomChar(0xd6)
    TITLE_TITLE = CustomChar(0x0e)
else:
    ARTIST_TITLE = 'Art:'
    ALBUM_TITLE = 'Alb:'
    TITLE_TITLE = ''


def catchall_signal_handler(*args, **kwargs):
    print("Caught signal (in catchall handler) "
           + kwargs['dbus_interface'] + "." + kwargs['member'])
    print(f"path:{kwargs['path']}")
    print(f"sender:{kwargs['sender']}")
    for arg in args:
        print("        " + str(arg))


class HIDPlayer():
    """Represents the state of the current player.

    Attributes
    ----------
    Attributes

    Methods
    -------
    Methods

    """
    def __init__(self):
        "Create a new player"
        self.initialized = False

def format_position(position):
    "Return a formatted string from position in microseconds."
    d = timedelta(milliseconds=round(position / 1000000) * 1000)
    formatted_str = str(d)
    if formatted_str[:2] == '0:':
        return formatted_str[2:]
    return formatted_str


class PositionText():
    """Update the Media position string

    Attributes
    ----------
    length : int
        length of the media in microseconds
    dbus_object : DBusObject
        DBus object to call to get information
    """
    def __init__(self, dbus_object, length):
        "docstring"
        self.dbus_object = dbus_object
        self.length = format_position(length)

    def __str__(self):
        position = self.dbus_object.Get('org.mpris.MediaPlayer2.Player', 'Position', dbus_interface='org.freedesktop.DBus.Properties')
        position = format_position(position)
        if len(position) < len(self.length):
            position.rjust(' ')
        return f'{position}/{self.length} '




class HIDMedia(HIDModule):
    """Send media information through HID

    Full description

    Attributes
    ----------
    sleep_time : int
        time between two fetching.
    """
    RESERVED_KEY = 0

    def __init__(self):
        "Create the media monitoring."
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SessionBus()
        self.dbus_object = None
        self.player = HIDPlayer()

        self.init_dbus()

        if self.dbus_object:
            self.init_player()

        # for debugging
        # self.bus.add_signal_receiver(catchall_signal_handler,
        #                              dbus_interface="org.freedesktop.DBus.Properties",
        #                              signal_name="PropertiesChanged",
        #                              path='/org/mpris/MediaPlayer2',
        #                              interface_keyword='dbus_interface', member_keyword='member', path_keyword='path', sender_keyword="sender")
        self.bus.add_signal_receiver(self.properties_changed,
                                     dbus_interface="org.freedesktop.DBus.Properties",
                                     signal_name="PropertiesChanged",
                                     path='/org/mpris/MediaPlayer2')


        self._lines = []
        self._check_lines = []

        super().__init__(0.5)
        self.screen = 2

        loop = gobject.MainLoop()
        self.glib_thread = threading.Thread(target=loop.run)
        self.glib_thread.start()

    def init_dbus(self):
        for service in self.bus.list_names():
            if service.startswith('org.mpris.MediaPlayer2.'):
                self.dbus_object = dbus.SessionBus().get_object(service, '/org/mpris/MediaPlayer2')

    def init_player(self):
        if not self.dbus_object:
            self.init_dbus()

        if self.dbus_object:
            # Init the player
            prop = dbus.Interface(self.dbus_object, 'org.freedesktop.DBus.Properties')
            properties = prop.GetAll('org.mpris.MediaPlayer2.Player')
            for p in properties:
                setattr(self.player, p, prop.Get('org.mpris.MediaPlayer2.Player', p))

            properties = prop.GetAll('org.mpris.MediaPlayer2')
            for p in properties:
                setattr(self.player, p, prop.Get('org.mpris.MediaPlayer2', p))

            self.player.initialized = True

    def properties_changed(self, name, metadata, signature):
        if not self.player.initialized:
            self.init_player()

        "Callback when changes in playing occurs."
        log.debug(f'New properties from {name} ; {signature}')
        for m in metadata:
            log.debug(f'Setting {m}: {metadata[m]}')
            setattr(self.player, m, metadata[m])

    def get_status(self):
        "Returns True if `get_text` should be called."
        if hasattr(self.player, 'PlaybackStatus'):
            return self.player.PlaybackStatus == 'Playing'
        return False
        # status = self.dbus_object.Get('org.mpris.MediaPlayer2.Player', 'PlaybackStatus', dbus_interface='org.freedesktop.DBus.Properties')
        # return status == 'Playing'

    def get_lines(self):
        """Returns the text to be displayed on screen.

        Parameters
        ----------
        Parameters

        Returns
        -------
        Returns
        """
        lines = []
        if hasattr(self.player, 'Metadata'):
            metadata = self.player.Metadata
        else:
            return

        can_seek = False
        if hasattr(self.player, 'CanSeek'):
            can_seek = self.player.CanSeek

        # metadata = self.dbus_object.Get('org.mpris.MediaPlayer2.Player', 'Metadata', dbus_interface='org.freedesktop.DBus.Properties')
        # can_seek = self.dbus_object.Get('org.mpris.MediaPlayer2.Player', 'CanSeek', dbus_interface='org.freedesktop.DBus.Properties')
        if can_seek:
            length = format_position(metadata['mpris:length'])
            lines.append([PositionText(self.dbus_object, metadata['mpris:length']), ScrollingText(self.player.Identity, conf.N_COLS - len(length) * 2 - 1)])
        else:
            lines.append(self.player.Identity)

        if "xesam:artist" in metadata:
            artist = str(metadata["xesam:artist"][0])
        else:
            artist = '-'

        if "xesam:album" in metadata:
            album = str(metadata["xesam:album"])
        else:
            album = '-'

        if "xesam:title" in metadata:
            title = str(metadata["xesam:title"])
        elif "xesam:url" in metadata:
            title = os.path.basename(metadata["xesam:url"])
        else:
            title = '-'

        lines.append([ARTIST_TITLE, ' ', ScrollingText(artist, conf.N_COLS - len(ARTIST_TITLE) - 1)])
        lines.append([ALBUM_TITLE, ' ', ScrollingText(album, conf.N_COLS - len(ALBUM_TITLE) - 1)])
        lines.append([TITLE_TITLE, ' ', ScrollingText(title, conf.N_COLS - len(TITLE_TITLE) - 1)])

        h = artist + album + title
        if self._check_lines != h:
            self._check_lines = h
            self._lines = lines

        return (self._lines, ["rjust"] + ["ljust"] * 3)
